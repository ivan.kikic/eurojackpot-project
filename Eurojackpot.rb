puts "Dobrodošli u eurojackpot!"

first_numbers = [] # Prvih 5 korisnikovih brojeva
second_numbers = [] # Druga 2 dodatna broja
jackpot_first_numbers = [] # Polje koje se popunjava random generiranim brojevima
jackpot_second_numbers = [] # Druga dva broja

puts "Unesite 5 brojeva iz intervala 1-50"


while first_numbers.size != 5 # Petlja koja se izvršava 5 puta, koliko treba korisnik unjeti
	loop do # Petlja koja uz if trazi unos 5 brojeva u zadanom intervalu sve dok korisnik ne unese ispravno
		$a = gets.chomp.to_i
		break if $a > 0 && $a < 51 # Uvijet koji zahtjeva unos brojeva 1-50
	end
	first_numbers << $a
	first_numbers.uniq! # Svaki broj koji se ponovi vec u izvlacenju ne stavi ga u polje
end


puts "Odlično! Sada unesite dva dodatna broja"


while second_numbers.size != 2 # Petlja koja se izvršava 5 puta, koliko treba korisnik unjeti
	loop do # Petlja koja uz if trazi unos 5 brojeva u zadanom intervalu sve dok korisnik ne unese ispravno
		$a = gets.chomp.to_i
		# break if $a > 0 && $a < 11 # Isto kao za prehodni slučaj
		#VEDRAN: Ruby je zakon:
		puts (1..10).to_a.include?($a)
		break if (1..10).to_a.include?($a)
	end
	# second_numbers << $a
	# second_numbers.uniq!
	# Ovo je ok, kao i gore ali ruby way je možda više ovako:
	second_numbers << $a unless second_numbers.include?($a)
end

#VEDRAN: Ovu "grafiku" bi možda stavio u funkciju jer vidim da ti se ponavlja
# puts "=============================="
# puts "=============================="
def prettyPrint(text="") # text="" - U slučaju da ne daš text, text će biti ""
	print "\n#{('='*30+"\n")*2}#{text}"
end


# puts "Vaši brojevi su: " # Jednostavno pomoću .each ispisujemo sve brojeve koje je korisnik unjeo
# 	first_numbers.sort.each { |n|
# 		print "#{n}  "
# 	}	

# puts "\nI dva dodatna broja: "

# 	second_numbers.sort.each { |n|
# 		print "#{n}  "
# 	}

#VEDRAN: Ovo sve gore možeš u jednom redu: 
prettyPrint("Vaši brojevi su: #{first_numbers.join(" ")}\nI dva dodatna broja: #{second_numbers.join(" ")}\nSretno!")

# puts "=============================="
# puts "=============================="
prettyPrint

a = Time.now # Spremamo u varijablu a i ispisujemo trenutno vrijeme kada je izvlačenje krenulo
puts Time.now
counter = 0 # Brojač pokušaja izvlačenja, kako bi na kraju izračunali koliko je trebalo izvlačenja do dobitka



while ((jackpot_first_numbers.sort != first_numbers.sort) || (jackpot_second_numbers.sort != second_numbers.sort)) do # Vrti nam ovu petlju dok ne budu korisnikovo polje i random polje jednaki
	# jackpot_first_numbers = [] # Nakon svakog neuspjelog izvlačenja vracamo polje jackpot brojeva na prazno polje kako bi moglo ponovno ispuniti ga
	# jackpot_second_numbers = []

	# while jackpot_first_numbers.size != 5 # Dok polje ne bude sadrzavalo 5 brojeva da izvlaci random brojeve
	# 	jackpot_first_numbers << rand(1..50)
	# 	jackpot_first_numbers.uniq! # Svaki broj koji se ponovi vec u izvlacenju ne stavi ga u polje
	# end

	#VEDRAN: Ovo gore i ovo dolje možeš ovako :)
	jackpot_first_numbers = (1..50).to_a.sample(5)
	jackpot_second_numbers = (1..10).to_a.sample(2)
	
	# while jackpot_second_numbers.size != 2 # isto kao za prvih 5 brojeva
	# 	jackpot_second_numbers << rand(1..10)
	# 	jackpot_second_numbers.uniq!
	# end
	counter = counter + 1 # Nakon svake petlje/izvlacenja povecamo broj pokusaja za 1
	print "\r#{counter} Izvlačenja - #{jackpot_first_numbers.join(",")}/#{second_numbers.join(",")}" if counter % 100000 == 0 #VEDRAN: da netko ne misli da ne radi :D
end

# VEDRAN: evo malo za one koji žele znati više :D
	# ovo ti daje array zajedničkih brojeva dva arraya - jackpot_first_numbers&first_numbers
	# s tim možda možeš u petlji: puts "UMALO: #{(jackpot_first_numbers&first_numbers).join(",")}" if (jackpot_first_numbers&first_numbers).size > 3
	# ili možda još bolje, srediti onaj uvjet za jackpot:
	# while (jackpot_first_numbers&first_numbers).size < 5 - nije možda kraće toliko ali možda nekad zatreba :D

	year = counter / 52 # Koliko godina bi trebalo da se izvuce ta kombinacija
	month = counter % 52 / 4 # Isto, ali za mjesece

	puts "ČESTITAM, OSVOJILI STE SVE PARE OVOG SVIJETA!!!" # :)
	puts "Trebalo je #{counter} izvlačenja do glavnog dobitka!" # Ispis potrebnog broja izvlačenja broja
	puts "To je #{year} godina i #{month} mjeseci." # Te koliko je to u godina i mjeseci
	puts "//////////////////////////"
	b = Time.now # varijabla b koja sadržava trenutno vrijeme odnosno kada je završen kod
	puts Time.now # Ispise ga
	c = b - a # Koliko je vremena trajalo izvođenje
	puts "/////////////////////////"
	puts "Vrijeme izvođenja: "
	puts c # Koliko je vremena trajalo izvođenje